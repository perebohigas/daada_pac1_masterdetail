package edu.uoc.daada_pac1_masterdetail.model


import java.time.LocalDate
import java.time.Period
import java.util.Date
import kotlin.collections.ArrayList

object BookContent {

    val quantityFakeBooks: Int = 50
    var books: MutableList<BookItem> = ArrayList()

    init {
        for (i in 0..quantityFakeBooks) {
            books.add(BookItem((i+1), "Title"+(i+1).toString(),"Author"+(i+1).toString(), Date(),"Desription "+(i+1).toString(),""))
        }
    }

}