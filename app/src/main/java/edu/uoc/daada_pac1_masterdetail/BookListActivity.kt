package edu.uoc.daada_pac1_masterdetail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_book_list.*
import kotlinx.android.synthetic.main.book_list.*

import edu.uoc.daada_pac1_masterdetail.model.BookContent
import edu.uoc.daada_pac1_masterdetail.model.BookItem
import kotlinx.android.synthetic.main.book_list_even_element.view.*

class BookListActivity : AppCompatActivity() {

//     *** Exercise 1 ***
//    val numberOfFakeItems: Int = 50
    var mTwoPane: Boolean = false
    var listBookItems: List<BookItem> = BookContent.books
    var recyclerAdapter: SimpleItemRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_list)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        mTwoPane = checkIfFrameForBookDetailIsPresent()

        // *** Exercise 1 ***
/*        val listView: ListView = findViewById(R.id.list_of_books)
        fillUpListWithFakeData(listView)*/

        // *** Exercise 2 ***
        var linearLayoutManager = LinearLayoutManager(this)
        val recyclerView: RecyclerView = findViewById(R.id.list_of_books)
        recyclerView.layoutManager = linearLayoutManager
        recyclerAdapter = SimpleItemRecyclerViewAdapter (listBookItems, mTwoPane, this.supportFragmentManager, this)
        recyclerView.adapter = recyclerAdapter

//        *** Exercise 1 ***
/*        if (mTwoPane) {
            listView.setOnItemClickListener { _, _, _, _ ->
                val intent = Intent(this, BookDetailActivity::class.java)
                this.startActivity(intent)
            }
        } else {
            getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_for_book_details, BookDetailFragment())
                .commit()
        }*/

    }

//     *** Exercise 1 ***
/*    fun fillUpListWithFakeData(listView: ListView){
        var listItems:Array<String> = Array(this.numberOfFakeItems,
            { i -> ((i+1).toString()+ "\t\t\t\t" + "Item " + (i+1).toString()) }
        )
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems)
        listView.setAdapter(adapter)
    }*/

    fun checkIfFrameForBookDetailIsPresent(): Boolean {
        if (frame_for_book_details == null) {
            Log.i((R.string.app_name).toString(),"Book details is NOT been shown [small screen]")
            return false
        } else {
            Log.i((R.string.app_name).toString(),"Book details is been shown [big screen]")
            return true
        }
    }

    class SimpleItemRecyclerViewAdapter(private var listBookItems: List<BookItem>, private val mTwoPane: Boolean, private val fragmentManager: FragmentManager, private val context: Context) : RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

        private val EVEN = 0
        private val ODD = 1

        override fun getItemCount(): Int {
            return listBookItems.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.mItem = listBookItems[position]
            holder.mTitleView.text = listBookItems[position].title
            holder.mAuthorView.text = listBookItems[position].author

            // Guarda la posició actual a la vista del holder
            var bookItemPosition = position

            Log.i((R.string.app_name).toString(),"POSITION :${position}")

            holder.itemView.setOnClickListener { _ ->
                var currentPosition: Int

                // Obtindre la posició del llibre on s'ha fet click de la vista
                currentPosition = bookItemPosition
                Log.i((R.string.app_name).toString(), "The book item of the position ${currentPosition} has been clicked")

                if(mTwoPane) {
                    //Iniciar el fragment corresponent a la tablet, enviant l'argument de la posició seleccionada
                    val newFragment = BookDetailFragment().apply {
                        arguments = Bundle()
                        arguments!!.putInt("BOOK_ITEM_POSITION", currentPosition)
                    }
                    Log.i((R.string.app_name).toString(), "Add fragment to this activity to show the details of the book in position ${currentPosition}")
                    fragmentManager.beginTransaction()
                        .replace(R.id.frame_for_book_details, newFragment)
                        .commit()
                } else {
                    // Iniciar la activitat corresponent al mòbil, enviant l'argument de la posició seleccionada
                    Log.i((R.string.app_name).toString(), "Start BookDetailsActivity to show the details of the book in position ${currentPosition}")
                    val intent = Intent(context, BookDetailActivity::class.java)
                    intent.putExtra("BOOK_ITEM_POSITION", currentPosition.toString())
                    context.startActivity(intent)
                }

            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            var view: View? = null

            // Carrega un layout per elements parells i un altre per elements imparells
            if (viewType == EVEN) {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.book_list_even_element, parent, false)
            } else if (viewType == ODD) {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.book_list_odd_element, parent, false)
            }

            return ViewHolder(view!!)
        }

        override fun getItemViewType(position: Int): Int {

            // Retorna tipus parells o imparells en funció de la seva posició
            var type: Int
            if (position % 2 == 0) {
                type = EVEN
            } else {
                type = ODD
            }
            return type
        }

        class ViewHolder(elementView: View) : RecyclerView.ViewHolder(elementView) {
            lateinit var mItem: BookItem
            val mTitleView: TextView = elementView.titleView
            val mAuthorView: TextView = elementView.authorView
        }

        fun setItems(items: List<BookItem>) {
            listBookItems = items
            notifyDataSetChanged()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_book_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.by_title) {
            recyclerAdapter?.setItems(listBookItems.sortedWith(compareBy({ it.title })))
            return true
        } else if (item.itemId == R.id.by_author) {
            recyclerAdapter?.setItems(listBookItems.sortedWith(compareBy({ it.author })))
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

}
