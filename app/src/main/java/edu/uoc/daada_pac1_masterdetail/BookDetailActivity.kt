package edu.uoc.daada_pac1_masterdetail

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import edu.uoc.daada_pac1_masterdetail.model.BookContent
import edu.uoc.daada_pac1_masterdetail.model.BookItem
import kotlinx.android.synthetic.main.activity_book_detail.*
import kotlinx.android.synthetic.main.book_details.*
import kotlinx.android.synthetic.main.book_details.view.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class BookDetailActivity : AppCompatActivity() {

    lateinit var book: BookItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_detail)
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        // Afegir fletxa superior per torna a la activity BookListActivity (definit al manifest)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val bookPosition: Int = intent.getStringExtra("BOOK_ITEM_POSITION").toInt()

        book = BookContent.books[bookPosition]

        author.text = book.author
        var dateFormatter = SimpleDateFormat("dd/MM/yyyy")
        publicationDate.text = dateFormatter.format(book.publicationDate)
        description.text = book.description

    }



}
