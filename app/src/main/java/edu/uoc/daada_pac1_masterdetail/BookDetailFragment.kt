package edu.uoc.daada_pac1_masterdetail


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import edu.uoc.daada_pac1_masterdetail.model.BookContent
import edu.uoc.daada_pac1_masterdetail.model.BookItem
import kotlinx.android.synthetic.main.book_details.*
import kotlinx.android.synthetic.main.book_details.view.*
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 *
 */
class BookDetailFragment : Fragment() {

    lateinit var book: BookItem

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        val bookPosition: Int = arguments?.getInt("BOOK_ITEM_POSITION") ?: 0

        book = BookContent.books[bookPosition]

        val hostView = inflater.inflate(R.layout.book_details, container, false)

        hostView.author.text = book.author
        var dateFormatter = SimpleDateFormat("dd/MM/yyyy")
        hostView.publicationDate.text = dateFormatter.format(book.publicationDate)

        hostView.description.text = book.description

        return hostView
    }

}
